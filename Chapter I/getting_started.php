<?php 
    $arr = array("index.php"=>"Home", 
    "product.php" => "Product", "service.php"=>"Services", 
    "about.php"=>"About", "contact.php" => "Contact");
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.js">
    </script>
    <style type="text/css">
        ul{
            list-style-type: none;
        }
        ul li 
        {
            display: inline;
        }
        ul li a{
            text-decoration: none;
            padding: 15px 10px;
            background-color: red;
            color:white;
            font-weight: bold;
            font-size: 16pt;
            border-radius: 10px 10px;
        }
        ul li a:hover{
            background-color: yellow;
        }
        
    </style>
    <script type="text/javascript">
        $(function(){
            $("#_sum").click(function(){
                var num1 = document.getElementById("txt_num1").value;
                var num2 = document.getElementById("txt_num2").value;
                var result = parseInt(num1) + parseInt(num2);
                $("#_result").text(result);
            });
        });
    </script>
</head>
<body>

    <main>

        <header>
            <ul>

                <?php 
                    foreach ($arr as $key => $value) {
                        ?>
                        <li><a href="<?php echo $key; ?>"><?php echo $value; ?></a></li>
                        <?php
                    }
                ?>
                <!-- <li>
                    <a href="index.php">Home</a>
                </li>
                <li>
                    <a href="index.php">Product</a>
                </li>
                <li>
                    <a href="index.php">Services</a>
                </li>
                <li>
                    <a href="index.php">About</a>
                </li>
                <li>
                    <a href="index.php">Contact</a>
                </li> -->
            </ul>
        </header>
        <div style="margin-top: 15px;">
            <h1>Getting started PHP Programming Language</h1>
        </div>
        <hr/>
        <div>
            <form>
                <label>Num1: <input type="number" id="txt_num1" /></label>
                <label>Num2: <input type="number" id="txt_num2" /></label>
            </form>
            <div style="margin-top: 20px;">
                <button id="_sum">Sum(+)</button>
            </div>

            <div>
                <p>Result is: <span id="_result"></span>
                </script></p>
            </div>
            
        </div>




    </main>
</body>
</html>