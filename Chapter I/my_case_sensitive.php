<?php
    /* 
        This is a function, 
        IT's for case sensitive 
        i create my_sum() function with small case, but we can call with upper case
    */
    function my_sum($n1, $n2)
    {
        return $n1 + $n2; // Calcuate by sum of two value from argument
    }

    $result = My_SUM(10, 20);

    echo "<h1>Result is: $result</h1>";
?>